/*
 * Copyright 2024 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"fmt"
	"net/http"
	"time"

	cassandra "gitlab.wikimedia.org/repos/generated-data-platform/cassandra-http-gateway"
	"schneider.vip/problem"
)

// Helper function for creating projections with Fields that only rely on the attribute name.
func names(names ...string) []cassandra.Field {
	fields := make([]cassandra.Field, 0)
	for _, name := range names {
		fields = append(fields, cassandra.Field{Column: name})
	}
	return fields
}

// Helper function for creating projections of Fields with aliased names.
func mappedNames(names map[string]string) []cassandra.Field {
	fields := make([]cassandra.Field, 0)
	for k, v := range names {
		fields = append(fields, cassandra.Field{Column: k, Alias: v})
	}
	return fields
}

// Helper to parse RFC3339 strings to time.Time; On failure, returns a prefilled *Problem
func parseTime(tstamp string) (time.Time, *problem.Problem) {
	var err error
	var t time.Time

	if t, err = time.Parse(time.RFC3339, tstamp); err != nil {
		return t, problem.New(
			problem.Status(http.StatusBadRequest),
			problem.Title("Invalid RFC3339 timestamp"),
			problem.Detail(fmt.Sprintf("Unable to parse timestamp: %s (%s)", tstamp, err)),
		)
	}

	return t, nil
}

// Cribbed from https://stackoverflow.com/a/59375088
func ternary[T any](cond bool, vtrue, vfalse T) T {
	if cond {
		return vtrue
	}
	return vfalse
}
