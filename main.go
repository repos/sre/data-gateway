/*
 * Copyright 2022 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	servicelib_cassandra "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/cassandra"
	servicelib_log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	cassandra "gitlab.wikimedia.org/repos/generated-data-platform/cassandra-http-gateway"
	"schneider.vip/problem"
)

var (
	// These values are assigned at build using `-ldflags` (see: Makefile)
	buildDate = "unknown"
	buildHost = "unknown"
	version   = "unknown"
)

var (
	reqCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Count of HTTP requests processed, partitioned by status code and HTTP method.",
		},
		[]string{"code", "method"},
	)

	durationHisto = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "A histogram of latencies for requests, partitioned by status code and HTTP method.",
			Buckets: []float64{.001, .0025, .0050, .01, .025, .050, .10, .25, .50, 1, 2.5, 5, 10, 25},
		},
		[]string{"code", "method"},
	)
	promBuildInfoGauge = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name:        "cassandra_data_gateway_build_info",
			Help:        "Build information",
			ConstLabels: map[string]string{"version": version, "build_date": buildDate, "build_host": buildHost, "go_version": runtime.Version()},
		})
)

func init() {
	prometheus.MustRegister(reqCounter, durationHisto, promBuildInfoGauge)
	promBuildInfoGauge.Set(1)
}

// Entrypoint for our service
func main() {
	var confFile = flag.String("config", "./config.yaml", "Path to the configuration file")

	var config *Config
	var err error
	var logger *servicelib_log.Logger

	flag.Parse()

	if config, err = ReadConfig(*confFile); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if logger, err = servicelib_log.NewLogger(os.Stdout, config.ServiceName, config.LogLevel); err != nil {
		fmt.Fprintf(os.Stderr, "Unable to initialize the logger: %s", err)
		os.Exit(1)
	}

	// Allow overriding config using environment variables
	MergeEnvironment(config, logger)

	logger.Info(
		"Initializing service %s (Version: %s, Go: %s, Build host: %s, Timestamp: %s",
		config.ServiceName,
		version,
		runtime.Version(),
		buildHost,
		buildDate,
	)

	logger.Info("Connecting to Cassandra database: %s (port %d)", strings.Join(config.Cassandra.Hosts, ","), config.Cassandra.Port)
	logger.Debug("Cassandra: configured for consistency level '%s'", strings.ToLower(config.Cassandra.Consistency))
	logger.Debug("Cassandra: configured for local datacenter '%s'", config.Cassandra.LocalDC)

	var cluster *gocql.ClusterConfig
	var session *gocql.Session

	cluster = newCassandraCluster(config)
	cluster.ConnectObserver = &servicelib_cassandra.LoggingConnectObserver{Logger: logger}

	if session, err = cluster.CreateSession(); err != nil {
		logger.Error("Failed to create Cassandra session: %s", err)
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	logger.Info("Finished connecting to Cassandra database")

	router := httprouter.New()
	builder := cassandra.SelectBuilder.Logger(logger).Session(session).CounterVec(reqCounter).HistogramVec(durationHisto)

	// Image Suggestions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	router.GET("/public/image_suggestions/suggestions/:wiki/:page_id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			From("image_suggestions", "suggestions").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/private/image_suggestions/feedback/:wiki/:page_id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			From("image_suggestions", "feedback").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/private/image_suggestions/title_cache/:wiki/:title", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			From("image_suggestions", "title_cache").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/private/image_suggestions/instanceof_cache/:wiki/:page_id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			From("image_suggestions", "instanceof_cache").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	// Commons Impact Metrics ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	router.GET("/public/commons/category_metrics_snapshot/:category/:start/:end", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var start, end time.Time
		var problemObj *problem.Problem

		if start, problemObj = parseTime(ps.ByName("start")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}
		if end, problemObj = parseTime(ps.ByName("end")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}

		builder.
			Select(
				mappedNames(
					map[string]string{
						"dt":                         "timestamp",
						"media_file_count":           "media-file-count",
						"media_file_count_deep":      "media-file-count-deep",
						"used_media_file_count":      "used-media-file-count",
						"used_media_file_count_deep": "used-media-file-count-deep",
						"leveraging_wiki_count":      "leveraging-wiki-count",
						"leveraging_wiki_count_deep": "leveraging-wiki-count-deep",
						"leveraging_page_count":      "leveraging-page-count",
						"leveraging_page_count_deep": "leveraging-page-count-deep",
					})...).
			From("commons", "category_metrics_snapshot").
			Where("category", cassandra.EQ, ps.ByName("category")).
			Where("dt", cassandra.GTE, start).
			Where("dt", cassandra.LT, end).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/media_file_metrics_snapshot/:media_file/:start/:end", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var start, end time.Time
		var problemObj *problem.Problem

		if start, problemObj = parseTime(ps.ByName("start")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}
		if end, problemObj = parseTime(ps.ByName("end")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}

		builder.
			Select(
				mappedNames(
					map[string]string{
						"dt":                    "timestamp",
						"leveraging_wiki_count": "leveraging-wiki-count",
						"leveraging_page_count": "leveraging-page-count",
					})...).
			From("commons", "media_file_metrics_snapshot").
			Where("media_file", cassandra.EQ, ps.ByName("media_file")).
			Where("dt", cassandra.GTE, start).
			Where("dt", cassandra.LT, end).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/pageviews_per_category_monthly/:category/:category_scope/:wiki/:start/:end", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var start, end time.Time
		var problemObj *problem.Problem

		if start, problemObj = parseTime(ps.ByName("start")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}
		if end, problemObj = parseTime(ps.ByName("end")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}

		builder.
			Select(
				mappedNames(
					map[string]string{
						"dt":             "timestamp",
						"pageview_count": "pageview-count",
					})...).
			From("commons", "pageviews_per_category_monthly").
			Where("category", cassandra.EQ, ps.ByName("category")).
			Where("category_scope", cassandra.EQ, ps.ByName("category_scope")).
			Where("wiki", cassandra.EQ, ps.ByName("wiki")).
			Where("dt", cassandra.GTE, start).
			Where("dt", cassandra.LT, end).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/pageviews_per_media_file_monthly/:media_file/:wiki/:start/:end", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var start, end time.Time
		var problemObj *problem.Problem

		if start, problemObj = parseTime(ps.ByName("start")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}
		if end, problemObj = parseTime(ps.ByName("end")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}

		builder.
			Select(
				mappedNames(
					map[string]string{
						"dt":             "timestamp",
						"pageview_count": "pageview-count",
					})...).
			From("commons", "pageviews_per_media_file_monthly").
			Where("media_file", cassandra.EQ, ps.ByName("media_file")).
			Where("wiki", cassandra.EQ, ps.ByName("wiki")).
			Where("dt", cassandra.GTE, start).
			Where("dt", cassandra.LT, end).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/edits_per_category_monthly/:category/:category_scope/:edit_type/:start/:end", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var start, end time.Time
		var problemObj *problem.Problem

		if start, problemObj = parseTime(ps.ByName("start")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}
		if end, problemObj = parseTime(ps.ByName("end")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}

		builder.
			Select(
				mappedNames(
					map[string]string{
						"dt":         "timestamp",
						"edit_count": "edit-count",
					})...).
			From("commons", "edits_per_category_monthly").
			Where("category", cassandra.EQ, ps.ByName("category")).
			Where("category_scope", cassandra.EQ, ps.ByName("category_scope")).
			Where("edit_type", cassandra.EQ, ps.ByName("edit_type")).
			Where("dt", cassandra.GTE, start).
			Where("dt", cassandra.LT, end).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/edits_per_user_monthly/:user_name/:edit_type/:start/:end", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var start, end time.Time
		var problemObj *problem.Problem

		if start, problemObj = parseTime(ps.ByName("start")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}
		if end, problemObj = parseTime(ps.ByName("end")); problemObj != nil {
			problemObj.WriteTo(w)
			return
		}

		builder.
			Select(
				mappedNames(
					map[string]string{
						"dt":         "timestamp",
						"edit_count": "edit-count",
					})...).
			From("commons", "edits_per_user_monthly").
			Where("user_name", cassandra.EQ, ps.ByName("user_name")).
			Where("edit_type", cassandra.EQ, ps.ByName("edit_type")).
			Where("dt", cassandra.GTE, start).
			Where("dt", cassandra.LT, end).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_pages_per_category_monthly/:category/:category_scope/:wiki/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"page_title":     "page-title",
						"pageview_count": "pageview-count",
						"rank":           "rank",
					})...).
			From("commons", "top_pages_per_category_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_wikis_per_category_monthly/:category/:category_scope/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"wiki":           "wiki",
						"pageview_count": "pageview-count",
						"rank":           "rank",
					})...).
			From("commons", "top_wikis_per_category_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_viewed_categories_monthly/:category_scope/:wiki/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"category":       "category",
						"pageview_count": "pageview-count",
						"rank":           "rank",
					})...).
			From("commons", "top_viewed_categories_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_pages_per_media_file_monthly/:media_file/:wiki/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"page_title":     "page-title",
						"pageview_count": "pageview-count",
						"rank":           "rank",
					})...).
			From("commons", "top_pages_per_media_file_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_wikis_per_media_file_monthly/:media_file/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"wiki":           "wiki",
						"pageview_count": "pageview-count",
						"rank":           "rank",
					})...).
			From("commons", "top_wikis_per_media_file_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_viewed_media_files_monthly/:category/:category_scope/:wiki/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"media_file":     "media-file",
						"pageview_count": "pageview-count",
						"rank":           "rank",
					})...).
			From("commons", "top_viewed_media_files_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_edited_categories_monthly/:category_scope/:edit_type/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"category":   "category",
						"edit_count": "edit-count",
						"rank":       "rank",
					})...).
			From("commons", "top_edited_categories_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.GET("/public/commons/top_editors_monthly/:category/:category_scope/:edit_type/:year/:month", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		builder.
			Select(
				mappedNames(
					map[string]string{
						"user_name":  "user-name",
						"edit_count": "edit-count",
						"rank":       "rank",
					})...).
			From("commons", "top_editors_monthly").
			Bind(ps).
			Build().
			Handle(w, r)
	})

	router.Handler("GET", "/healthz", &HealthzHandler{NewHealthz(version, buildDate, buildHost), logger})
	router.Handler("GET", "/metrics", promhttp.Handler())
	router.Handler("GET", "/openapi", openAPIHandler(logger))

	addr := net.JoinHostPort(config.Address, strconv.Itoa(config.Port))

	logger.Info("Listening on %s", addr)

	logger.Fatal("%v", http.ListenAndServe(addr, router))
}
