module gitlab.wikimedia.org/repos/sre/data-gateway

go 1.18

require (
	gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang v1.0.0
	github.com/gocql/gocql v1.6.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/prometheus/client_golang v1.19.1
	github.com/stretchr/testify v1.9.0
	gitlab.wikimedia.org/repos/generated-data-platform/cassandra-http-gateway v1.0.3
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.9.1
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_model v0.5.0 // indirect
	github.com/prometheus/common v0.48.0 // indirect
	github.com/prometheus/procfs v0.12.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
