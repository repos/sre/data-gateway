# Data Gateway Service

Provide read-only access to datasets (currently via the [cassandra-http-gateway]
framework) for the [Data Gateway Service][Data Gateway].

## Building

Building requires:

- Go (>= 1.21)
- Make

```sh-session
$ make
...
$
```

### Enabling debug for gocql (Cassandra driver)

```sh-session
$ make GOBUILD_ARGS="-tags gocql_debug"
...
$
```

## Usage

```sh-session
$ ./data-gateway -h
Usage of ./data-gateway:
  -config string
    	Path to the configuration file (default "./config.yaml")
$
```

Running the service requires access to a [Cassandra] database.  Schema can be recreated using
the files in `schema.d/` (ala `for f in schema.d/*; do cqlsh -f $f; done`), but (for the time
being) test data is left as an exercise for the reader.


## Configuration

See the well-commented example, `config.yaml`.


## Deployment

See: https://wikitech.wikimedia.org/wiki/Kubernetes/Deployments


[Cassandra]:              http://cassandra.apache.org
[cassandra-http-gateway]: https://gitlab.wikimedia.org/repos/generated-data-platform/cassandra-http-gateway
[Data Gateway]:           https://www.mediawiki.org/wiki/Data_Gateway
[T293807]:                https://phabricator.wikimedia.org/T293807 
