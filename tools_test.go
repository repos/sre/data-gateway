/*
 * Copyright 2024 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	cassandra "gitlab.wikimedia.org/repos/generated-data-platform/cassandra-http-gateway"
	"schneider.vip/problem"
)

func TestParseGoodTimestamp(t *testing.T) {
	var err error
	var probObj *problem.Problem
	var tsA, tsB time.Time

	tsA, err = time.Parse(time.RFC3339, "2024-06-05T14:33:32-05:00")
	require.NoError(t, err)

	tsB, probObj = parseTime("2024-06-05T14:33:32-05:00")
	require.Nil(t, probObj)

	// Compare results
	assert.Equal(t, tsA, tsB)
}

func TestParseInvalidTimestamp(t *testing.T) {
	var data []byte
	var err error
	var probObj *problem.Problem

	_, probObj = parseTime("2024-06-05 14:33:3")
	require.NotNil(t, probObj)

	// Marshal problem object to JSON, and then Unmarshal it for inspection
	data, err = probObj.MarshalJSON()
	require.NoError(t, err)

	jsonObj := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
		Detail string `json:"detail"`
	}{}

	err = json.Unmarshal(data, &jsonObj)
	require.NoError(t, err)
	assert.Equal(t, jsonObj.Title, "Invalid RFC3339 timestamp")
	assert.Equal(t, jsonObj.Status, http.StatusBadRequest)
}

func TestTernaryExpression(t *testing.T) {
	condition := func() bool { return true }

	assert.True(t, ternary(condition(), true, false))
	assert.True(t, ternary(!condition(), false, true))
	assert.Equal(t, "a", ternary(condition(), "a", "b"))
}

func TestNames(t *testing.T) {
	var n []cassandra.Field = names("foo", "bar", "baz")
	require.Len(t, n, 3)

	assert.Equal(t, n[0].Column, "foo")
	assert.Equal(t, n[1].Column, "bar")
	assert.Equal(t, n[2].Column, "baz")
}

func TestMappedNames(t *testing.T) {
	var n []cassandra.Field = mappedNames(
		map[string]string{
			"apple_banana":    "apple-banana",
			"carrot_cucumber": "carrot-cucumber",
			"kiwi_pineapple":  "kiwi-pineapple",
		})
	require.Len(t, n, 3)

	for _, v := range n {
		switch v.Column {
		case "apple_banana":
			assert.Equal(t, v.Alias, "apple-banana")
		case "carrot_cucumber":
			assert.Equal(t, v.Alias, "carrot-cucumber")
		case "kiwi_pineapple":
			assert.Equal(t, v.Alias, "kiwi-pineapple")
		default:
			t.Errorf("Unknown column name: %s", v.Column)
		}
	}
}
